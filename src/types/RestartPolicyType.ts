const NO = 'no'
const UNLESS_STOPPED = 'unless-stopped'
const ON_FAILURE =  'on-failure'
const ALWAYS = 'always'

export const RestartPolicyTypes = { NO, UNLESS_STOPPED, ON_FAILURE, ALWAYS }
export type RestartPolicyType = 'no' | 'unless-stopped' | 'on-failure' | 'always'
export type RestartPolicyTypeUppercase = keyof typeof RestartPolicyTypes