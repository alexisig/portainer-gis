const CONTAINER = 1
const SWARM_STACK = 2
const COMPOSE_STACK = 3
export const TYPES = { CONTAINER, SWARM_STACK, COMPOSE_STACK }
export type TemplateType = 1 | 2 | 3
export type TemplateTypeString = keyof typeof TYPES
