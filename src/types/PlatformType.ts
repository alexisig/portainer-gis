const WINDOWS = 'windows'
const LINUX = 'linux'
export const PLATFORMS = { WINDOWS, LINUX }
export type PlatformType = 'windows' | 'linux'
export type PlatformTypeUpperCase = keyof typeof PLATFORMS