import { TemplateType, TemplateTypeString, TYPES } from "./types/TemplateType";
import { PlatformType, PlatformTypeUpperCase } from "./types/PlatformType";
import { PortType } from "./types/PortType";
import { RestartPolicyType, RestartPolicyTypeUppercase } from "./types/RestartPolicyType";
import { EnvProperty, SimplifiedEnvPropertyType } from "./models/EnvProperty";
import { LabelProperty, SimplifiedLabelPropertyType } from "./models/LabelProperty";
import VolumeProperty from "./models/VolumeProperty";

const getTraefikNameValueOrDefault = ([name, value], templateName, fns) => {
    if (Object.keys(fns).includes(name)) {
        return {
            name: fns[name](templateName).toLowerCase(),
            value: value
        }
    }
    return { name, value }
}

interface TemplateInterface {
    type: TemplateTypeString | TemplateType;
    title: string;
    description: string;
    logo: string;
    image: string;
    administrator_only?: Boolean;
    name?: string;
    registry?: string;
    command?: string;
    env?: (EnvProperty | SimplifiedEnvPropertyType)[]
    network?: string;
    volume?: VolumeProperty[];
    ports?: PortType[];
    labels?: (LabelProperty | SimplifiedLabelPropertyType)[];
    privileged?: boolean;
    interactive?: boolean;
    restart_policy?: RestartPolicyType | RestartPolicyTypeUppercase;
    hostname?: string;
    note?: string;
    platform?: PlatformType | PlatformTypeUpperCase;
    categories?: string[];
    traefikHelpers: any;
}

class Template {
    type: TemplateTypeString | TemplateType;
    title: string;
    description: string;
    logo: string;
    image: string;
    administrator_only?: Boolean;
    name?: string;
    registry?: string;
    command?: string;
    env?: (EnvProperty | SimplifiedEnvPropertyType)[]
    network?: string;
    volume?: VolumeProperty[];
    ports?: PortType[];
    labels?: (LabelProperty | SimplifiedLabelPropertyType)[];
    privileged?: boolean;
    interactive?: boolean;
    restart_policy?: RestartPolicyType | RestartPolicyTypeUppercase;
    hostname?: string;
    note?: string;
    platform?: PlatformType | PlatformTypeUpperCase;
    categories?: string[];
    constructor({ 
        type, title, description, logo, image, 
        administrator_only, name, registry, command, 
        env, network, volume, ports, labels, privileged,
        interactive, restart_policy, hostname, note,
        platform, categories, traefikHelpers
     } : TemplateInterface) {
        this.type = <TemplateType>(typeof type === 'string' ? TYPES[type] : type)
        this.title = title
        this.description = description
        this.logo = logo
        this.image = image
        this.administrator_only = administrator_only
        this.name = name
        this.registry = registry
        this.command = command
        this.network = network
        this.volume = volume && volume.map(v => new VolumeProperty(v))
        this.ports = ports
        this.labels = labels && labels.map(l => {
            if (l instanceof LabelProperty) {
                return new LabelProperty(l)
            } else {
                return new LabelProperty(getTraefikNameValueOrDefault(l, title, traefikHelpers))
            }
        })
        this.privileged = privileged
        this.interactive = interactive
        this.restart_policy = restart_policy
        this.hostname = hostname
        this.note = note
        this.platform = <PlatformType>(platform && platform.toLowerCase())
        this.categories = categories
        this.env = env && env.map(e => {
            if (e instanceof EnvProperty) {
                return new EnvProperty(e)
            } else {
                return new EnvProperty({ name: e[0], label: e[1], default: e[2] })
            }
        })

    }
}

class PortainerTemplate {
    templates: Template[];
    traefikHelpers: any;
    constructor({ templates, traefikHelpers }: { templates: Template[], traefikHelpers: any}) {
        this.templates = templates.map(t => new Template({ traefikHelpers, ...t }))
    }
    export() {
        return JSON.stringify({ version: '2', templates: this.templates} , null, 4)
    }
}

export default PortainerTemplate