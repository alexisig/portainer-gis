class VolumeProperty {
    container: string;
    bind: string;
    readonly: boolean;
    constructor({ container, bind, readonly } : VolumeProperty) {
        this.container = container
        this.bind = bind
        this.readonly = readonly
    }
}

export default VolumeProperty