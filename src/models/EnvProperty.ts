import { CommonType } from "../types/CommonType";
import EnvPropertyOption from "./EnvPropertyOption";

export class EnvProperty {
    name: string;
    label: string;
    description?: string;
    default?: CommonType;
    preset?: boolean;
    select?: EnvPropertyOption[];
    constructor({ name, label, description, preset, select, ...others }: EnvProperty) {
        this.name = name
        this.label = label
        this.description = description
        this.preset = preset
        this.default = others.default
        this.select = select && select.map(s => new EnvPropertyOption(s))
    }
}

export type SimplifiedEnvPropertyType = [ string, string, CommonType? ]