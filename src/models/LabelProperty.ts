import { CommonType } from "../types/CommonType";

export class LabelProperty {
    name: string;
    value: CommonType;
    constructor({ name, value }: LabelProperty) {
        this.name = name
        this.value = value
    }
}

export type SimplifiedLabelPropertyType = [ string, CommonType ]