class EnvPropertyOption {
    text: string;
    value: any;
    default: any;
    constructor({ text, value, ...other } : EnvPropertyOption) {
        this.text = text
        this.value = value
        this.default = other.default
    }
}

export default EnvPropertyOption