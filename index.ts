import PortainerTemplate from "./src"
const fs = require('fs')

const instance = new PortainerTemplate({
    traefikHelpers : {
        tr_port: name => `traefik.http.services.${name}.loadbalancer.server.port`,
        tr_certresolver: name => `traefik.http.routers.${name}.tls.certresolver`,
        tr_rule: name => `traefik.http.routers.${name}.rule`,
        tr_entrypoints: name => `traefik.http.routers.${name}.entrypoints`
    },
    templates: [
        {
            type: 'CONTAINER',
            title: 'Qgis-Server',
            image: 'kartoza/qgis-server',
            logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/QGis_Logo.png/816px-QGis_Logo.png',
            description: 'Qgis server with apache.',
            network: "gateway",
            platform: 'LINUX',
            env: [
                    [
                        'QGIS_DEBUG', 
                        'QGIS_DEBUG', 
                        '5'
                    ],
                    [
                        'QGIS_LOG_FILE', 
                        'QGIS_LOG_FILE', 
                        '/proc/self/fd/1'
                    ],
                    [
                        'QGIS_SERVER_LOG_FILE', 
                        'QGIS_SERVER_LOG_FILE', 
                        '/proc/self/fd/1'
                    ],
                    [
                        'QGIS_SERVER_LOG_LEVEL', 
                        'QGIS_SERVER_LOG_LEVEL', 
                        '5'
                    ],
                    [
                        'PGSERVICEFILE', 
                        'PGSERVICEFILE',
                        '/project/pg_service.conf'
                    ],
                    [  
                        'QGIS_PROJECT_FILE',
                        'QGIS_PROJECT_FILE',
                        '/project/project.qgs'
                    ],
                    [
                        'QGIS_PLUGINPATH', 
                        'QGIS_PLUGINPATH', 
                        '/opt/qgis-server/plugins'
                    ],
            ], 
            labels: [
                ['tr_entrypoints', 'websecure'],
                ['tr_rule', 'Host(`qgis.alexisig.com`)'],
                ['tr_certresolver', 'le'],
                ['tr_port', "80"]
            ]
        }, {
            type: 'CONTAINER',
            title: 'Geoserver',
            image: 'kartoza/geoserver:2.16.2',
            logo: 'https://svn.osgeo.org/osgeo/marketing/logo/projects/geoserver/PNG/GeoServer_MARK.png',
            description: 'geoserver',
            network: "gateway",
            platform: 'LINUX',
            env: [
                [ 
                    'STABLE_EXTENSIONS', 
                    `Stable Extensions`,
                    '' 
                ],
                [ 
                    'COMMUNITY_EXTENSIONS',
                    'Community Extensions',
                    ''
                ],
                [
                    'SAMPLE_DATA',
                    'Sample Data by default', 
                    true
                ],
            ], 
            labels: [
                ['tr_entrypoints', 'websecure'],
                ['tr_rule', 'Host(`geoserver.alexisig.com`)'],
                ['tr_certresolver', 'le'],
                ['tr_port', "8080"]
            ]
        }, {
            type: 'CONTAINER',
            title: 'Overpass',
            image: 'wiktorn/overpass-api',
            logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Openstreetmap_logo.svg/256px-Openstreetmap_logo.svg.png',
            description: 'Overpass world API',
            network: "gateway",
            platform: 'LINUX',
            env: [
                [
                    'OVERPASS_META',
                    'OVERPASS_META',
                    'yes'
                ], [
                    'OVERPASS_MODE',
                    'OVERPASS_MODE',
                    'clone'
                ], [
                    'OVERPASS_DIFF_URL',
                    'OVERPASS_DIFF_URL',
                    'https://planet.openstreetmap.org/replication/minute/'
                ]
            ], 
            volume: [
                {
                    container: '/db',
                    bind: '/overpass',
                    readonly: false
                }
            ], 
            labels: [
                ['tr_entrypoints', 'websecure'],
                ['tr_rule', 'Host(`overpass.alexisig.com`)'],
                ['tr_certresolver', 'le'],
                ['tr_port', "80"]
            ]
        }
    ]
})

fs.writeFile('gis-templates.json', instance.export(), () => console.log('Exported'))